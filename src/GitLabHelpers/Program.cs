﻿using BarRaider.SdTools;
using Newtonsoft.Json;

namespace GitLabHelpers
{
    class Program
    {
        static void Main(string[] args)
        {
            //while (!System.Diagnostics.Debugger.IsAttached) { System.Threading.Thread.Sleep(100); }
            //System.Diagnostics.Debugger.Launch();
            SDWrapper.Run(args);
        }
    }
}
