using Newtonsoft.Json;

namespace GitLabHelpers
{
    internal class PluginSettings
    {
        internal static PluginSettings CreateDefault()
        {
            var instance = new PluginSettings();
            instance.GitLabAddress = "https://gitlab.com";
            instance.GitLabToken = "missingTokenHere";
            return instance;
        }

        [JsonProperty(PropertyName = "GitLabAddress")]
        public string GitLabAddress { get; set; }

        [JsonProperty(PropertyName = "GitLabToken")]
        public string GitLabToken { get; set; }
    }
}
