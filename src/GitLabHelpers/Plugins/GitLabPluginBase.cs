using System;
using System.Drawing;
using System.Threading.Tasks;
using BarRaider.SdTools;
using GitLabApiClient;
using Newtonsoft.Json.Linq;

namespace GitLabHelpers
{
    abstract class GitLabPluginBase : PluginBase
    {
        protected PluginSettings Settings { get; set; }

        protected GitLabClient GitLabClient { get; set; }

        protected string GitLabUserName { get; set; } = "";

        protected DateTime LastKeyPress = DateTime.Now;
        protected DateTime LastGitLabUpdate = DateTime.Now;
        protected const int KEY_PRESS_MIN_DELAY_SECONDS = 5;
        protected const int API_UPDATE_MIN_DELAY_SECONDS = 10;

        protected GitLabPluginBase(SDConnection connection, InitialPayload payload) : base(connection, payload)
        {
            if (payload == null || payload.Settings.Count == 0)
            {
                Settings = PluginSettings.CreateDefault();
            }
            else
            {
                Settings = payload.Settings.ToObject<PluginSettings>();
            }
            ConnectClient();
        }

        private void ConnectClient()
        {
            try
            {
                GitLabClient = new GitLabClient(Settings.GitLabAddress, Settings.GitLabToken);
                GitLabUserName = GitLabClient.Users.GetCurrentSessionAsync().Result.Username;
            }
            catch (ArgumentException ex)
            {
                Connection.LogSDMessage($"{ex.Message}");
                Logger.Instance.LogMessage(TracingLevel.ERROR, $"{ex.Message}");
            }
        }

        public override void ReceivedSettings(ReceivedSettingsPayload payload)
        {
            Tools.AutoPopulateSettings(Settings, payload.Settings);
            Connection.SetSettingsAsync(JObject.FromObject(Settings));
            ConnectClient();
        }

        protected async Task DrawString(string data, Image BaseImage)
        {
            var graphics = Graphics.FromImage(BaseImage);
            var point = data.Length > 1 ? new PointF(20, 40) : new PointF(40, 40);
            var pointS = data.Length > 1 ? new PointF(10, 34) : new PointF(32, 34);
            graphics.DrawString(data, new Font("Verdana", 76, FontStyle.Bold), Brushes.Black, pointS);
            graphics.DrawString(data, new Font("Verdana", 64, FontStyle.Bold), Brushes.Lime, point);
            string imgBase64 = Tools.ImageToBase64(BaseImage, true);
            await Connection.SetImageAsync(imgBase64);
            graphics.Dispose();
            BaseImage.Dispose();
        }
    }
}
