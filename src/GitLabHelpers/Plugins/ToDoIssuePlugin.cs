using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using BarRaider.SdTools;
using GitLabApiClient;
using GitLabApiClient.Models.ToDoList.Responses;

namespace GitLabHelpers
{
    [PluginActionId("net.jeremybeier.gitlabhelpers.todoissues.streamdeckplugin")]
    class ToDoIssuesPlugin : GitLabPluginBase
    {
        public ToDoIssuesPlugin(SDConnection connection, InitialPayload payload) : base(connection, payload) { }

        public override void KeyReleased(KeyPayload payload)
        {
            if (DateTime.Now - LastKeyPress < TimeSpan.FromSeconds(KEY_PRESS_MIN_DELAY_SECONDS))
                return;
            LastKeyPress = DateTime.Now;

            if (ToDos == null)
                return;
            if (ToDos.Count < 1)
                return;

            Process proc = new Process();
            proc.StartInfo.UseShellExecute = true;
            proc.StartInfo.FileName = $"{ToDos.First().TargetUrl}";
            proc.Start();
        }

        private IList<IToDo> ToDos = null;
        public async override void OnTick()
        {
            string display = "?";
            if (ToDos != null)
                display = ToDos.Count == 0 ? "." : ToDos.Count.ToString();
            var image = Image.FromFile("Images/issue@2x.png"); ;
            await DrawString($"{display}", image);
            if (DateTime.Now - LastGitLabUpdate < TimeSpan.FromSeconds(API_UPDATE_MIN_DELAY_SECONDS))
                return;
            LastGitLabUpdate = DateTime.Now;

            if (GitLabClient != null)
            {
                ToDos = await GitLabClient.ToDoList.GetAsync(o =>
                {
                    o.Type = ToDoTargetType.Issue;
                });
            }
            else
                ToDos = null;
        }
        public override void ReceivedGlobalSettings(ReceivedGlobalSettingsPayload payload) { }
        public override void KeyPressed(KeyPayload payload) { }
        public override void Dispose() { }
    }
}
